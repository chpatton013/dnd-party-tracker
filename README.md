# D&D Party Tracker

Use this application to keep track of the characters in your D&D party.

## Features

### Dynamic Character Sheet

update in realtime

view stats of entire party

dm gets updates as changes are made

### Different Party Renderers

single character view

full-party tracker for dm

### Party-wide Inventory Management

public/private personal inventory

shared inventory is distributed across party for realism

## Usage

```
# Start the server.
node src/index.js
# Access the root page.
curl localhost:3000
```

## Configuration

All configurable values are stored in various files under `./config`. These
files are loaded on server startup, so changes made after will not be
identified.

Each entry in the configuration files contains an environment variable name and
a default value. You can also provide a "user config directory" with your own
customizations. The resulting value for each entry is calculated as follows:

```
1. Environment variable, if set
2. User config value, if provided
3. Default value
```
