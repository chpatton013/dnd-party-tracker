'use strict';

var Utilities = require('../../src/utilities');

module.exports = {
   valid: {
      ability: {
         names: get_valid_ability_names,
      },
   },
   invalid: {
      ability: {
         names: get_invalid_ability_names,
         ids: get_invalid_ability_ids,
      },
   },
};

function get_null_types() {
   return [undefined, null];
}

function get_boolean_types() {
   return [true, false];
}

function get_compound_types() {
   return [[], {}];
}

function get_invalid_auto_increment_ids() {
   return ['', 'a', -1, 0];
}

function get_valid_ability_names(models) {
   return models.Ability.getEnumAbility();
}

function get_invalid_ability_names() {
   return ['', 'not part of the valid enum set', -1, 0, 1];
}

function get_invalid_ability_ids() {
   return [].concat(
         get_null_types(),
         get_boolean_types(),
         get_compound_types(),
         get_invalid_auto_increment_ids());
}
