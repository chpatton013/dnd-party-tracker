'use strict';

var path = require('path');
var Sequelize = require('sequelize');
var Config = require('../../src/config');
var Utilities = require('../../src/utilities');

module.exports = {
   config: create_config,
   sequelize: create_sequelize,
   create_context: create_context,
};

function create_config() {
   return Config(Utilities.project_path('test/config'));
}

function create_sequelize(config) {
   return new Sequelize(
         config.database.name,
         config.database.username,
         config.database.password,
         {
            host: config.database.host,
            port: config.database.port,
            dialect: config.database.dialect,
            storage: config.database.sqlite_storage_file,
         });
}

function create_context() {
   return create_config().then(function(config) {
      var sequelize = create_sequelize(config);

      var Models = require('./' + path.relative(
            __dirname, Utilities.project_path('src/models')));
      var Controllers = require('./' + path.relative(
            __dirname, Utilities.project_path('src/controllers')));

      var model_directory = Utilities.project_path(
            config.app.model_directory);
      var models = Models(model_directory, sequelize);

      var controller_directory = Utilities.project_path(
            config.app.controller_directory);
      var controllers = Controllers(controller_directory, models);

      return {
         config: config,
         sequelize: sequelize,
         models: models,
         controllers: controllers,
      };
   });
}
