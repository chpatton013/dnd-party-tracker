'use strict';

var Promise = require('bluebird');

var Chai = require('chai');
var assert = Chai.assert;
Chai.use(require('chai-as-promised'));

var Utilities = require('../../../src/utilities');
var TestUtilities = require('../../src/utilities');
var Data = require('../../src/data');

describe('Controller', function() {
   var models;
   var controllers;

   beforeEach(function() {
      return TestUtilities.create_context().then(function(context) {
         models = context.models;
         controllers = context.controllers;
         return context.sequelize.sync({force: true});
      });
   });

   describe('Ability', function() {
      var valid_names = function() {
         return Data.valid.ability.names(models);
      };
      var invalid_names = function() {
         return Data.invalid.ability.names();
      };
      var invalid_ids = function() {
         return Data.invalid.ability.ids();
      };

      describe('get_abilities', function() {
         function make_response(abilities) {
            return {
               abilities: abilities.map(Utilities.json.ability),
            };
         }

         it('should accept empty set', function() {
            return assert.eventually.deepEqual(
                  controllers.Ability.methods.get_abilities(),
                  make_response([]));
         });

         it('should accept single ability set', function() {
            return models.Ability.create({
               name: valid_names()[0],
            }).then(function(ability) {
               return assert.eventually.deepEqual(
                     controllers.Ability.methods.get_abilities(),
                     make_response([ability]));
            });
         });

         it('should accept multi-ability set', function() {
            return Promise.map(valid_names(), function(name) {
               return models.Ability.create({name: name});
            }).then(function(abilities) {
               return assert.eventually.deepEqual(
                     controllers.Ability.methods.get_abilities(),
                     make_response(abilities));
            });
         });
      });

      describe('get_ability', function() {
      });

      describe('create_ability', function() {
      });

      describe('modify_ability', function() {
      });
   });
});
