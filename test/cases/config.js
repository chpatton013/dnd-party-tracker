'use strict';

var path = require('path');
var fs = require('fs-promise');
var mktemp = require('mktemp');
var Promise = require('bluebird');

var Chai = require('chai');
var assert = Chai.assert;
Chai.use(require('chai-as-promised'));

var Config = require('../../src/config');

describe('App', function() {
   describe('Config', function() {
      function populate_from_object(directory, name, data) {
         var filename = path.join(directory, name + '.json');
         return fs.writeJson(filename, data).then(function() {
            return filename;
         });
      }

      function populate_from_string(directory, name, data) {
         var filename = path.join(directory, name + '.json');
         return fs.writeFile(filename, data).then(function() {
            return filename;
         });
      }

      function tempdir() {
         var tempdir_template = '/tmp/XXXXXX';
         return mktemp.createDir(tempdir_template);
      }

      function create_default_expected(expected) {
         var data = {};
         for (var i in expected) {
            var value = expected[i];
            data[i] = {
               environment_variable: value,
               default_value: value,
            };
         }
         return data;
      }

      function assert_config_object(args) {
         var default_directory = args[0];
         var user_directory = args[1];
         var expected = args[2];
         var config = Config(default_directory, user_directory);
         return assert.eventually.deepEqual(config, expected);
      }

      it('empty default and user directories', function() {
         return Promise.join(tempdir(), tempdir(), {}).then(assert_config_object);
      });

      it('single default file and no user files', function() {
         return Promise.join(tempdir(), tempdir()).then(function(args) {
            var default_directory = args[0];
            var user_directory = args[1];
            var base_expected = {a: 'a', b: 'b'};
            var expected = create_default_expected(base_expected);
            return Promise.join(
                  default_directory,
                  user_directory,
                  {a: base_expected},
                  populate_from_object(default_directory, 'a', expected));
         }).then(assert_config_object);
      });

      it('multiple default files and no user files', function() {
         return Promise.join(tempdir(), tempdir()).then(function(args) {
            var default_directory = args[0];
            var user_directory = args[1];
            var base_expected = {a: 'a', b: 'b'};
            var expected = create_default_expected(base_expected);
            return Promise.join(
                  default_directory,
                  user_directory,
                  {a: base_expected, b: base_expected, c: base_expected},
                  populate_from_object(default_directory, 'a', expected),
                  populate_from_object(default_directory, 'b', expected),
                  populate_from_object(default_directory, 'c', expected));
         }).then(assert_config_object);
      });

      it('no default files and single user file', function() {
         return Promise.join(tempdir(), tempdir()).then(function(args) {
            var default_directory = args[0];
            var user_directory = args[1];
            var expected = {a: 'a', b: 'b'};
            return Promise.join(
                  default_directory,
                  user_directory,
                  {},
                  populate_from_object(user_directory, 'a', expected));
         }).then(assert_config_object);
      });

      it('no default files and multiple user files', function() {
         return Promise.join(tempdir(), tempdir()).then(function(args) {
            var default_directory = args[0];
            var user_directory = args[1];
            var expected = {a: 'a', b: 'b'};
            return Promise.join(
                  default_directory,
                  user_directory,
                  {},
                  populate_from_object(user_directory, 'a', expected),
                  populate_from_object(user_directory, 'b', expected),
                  populate_from_object(user_directory, 'c', expected));
         }).then(assert_config_object);
      });

      it('single default file and single user file', function() {
         return Promise.join(tempdir(), tempdir()).then(function(args) {
            var default_directory = args[0];
            var user_directory = args[1];
            var base_default_expected = {a: 'a', b: 'b'};
            var default_expected = create_default_expected(base_default_expected);
            var user_expected = {a: 'c', b: 'd'};
            return Promise.join(
                  default_directory,
                  user_directory,
                  {a: user_expected},
                  populate_from_object(default_directory, 'a', default_expected),
                  populate_from_object(user_directory, 'a', user_expected));
         }).then(assert_config_object);
      });

      it('multiple default files and multiple user files', function() {
         return Promise.join(tempdir(), tempdir()).then(function(args) {
            var default_directory = args[0];
            var user_directory = args[1];
            var base_default_expected = {a: 'a', b: 'b'};
            var default_expected = create_default_expected(base_default_expected);
            var user_expected = {a: 'c', b: 'd'};
            return Promise.join(
                  default_directory,
                  user_directory,
                  {a: user_expected, b: user_expected, c: user_expected},
                  populate_from_object(default_directory, 'a', default_expected),
                  populate_from_object(default_directory, 'b', default_expected),
                  populate_from_object(default_directory, 'c', default_expected),
                  populate_from_object(user_directory, 'a', user_expected),
                  populate_from_object(user_directory, 'b', user_expected),
                  populate_from_object(user_directory, 'c', user_expected));
         }).then(assert_config_object);
      });

      it('single default file and multiple user files', function() {
         return Promise.join(tempdir(), tempdir()).then(function(args) {
            var default_directory = args[0];
            var user_directory = args[1];
            var base_default_expected = {a: 'a', b: 'b'};
            var default_expected = create_default_expected(base_default_expected);
            var user_expected = {a: 'c', b: 'd'};
            return Promise.join(
                  default_directory,
                  user_directory,
                  {a: user_expected},
                  populate_from_object(default_directory, 'a', default_expected),
                  populate_from_object(user_directory, 'a', user_expected),
                  populate_from_object(user_directory, 'b', user_expected),
                  populate_from_object(user_directory, 'c', user_expected));
         }).then(assert_config_object);
      });

      it('multiple default files and single user file', function() {
         return Promise.join(tempdir(), tempdir()).then(function(args) {
            var default_directory = args[0];
            var user_directory = args[1];
            var base_default_expected = {a: 'a', b: 'b'};
            var default_expected = create_default_expected(base_default_expected);
            var user_expected = {a: 'c', b: 'd'};
            return Promise.join(
                  default_directory,
                  user_directory,
                  {a: user_expected, b: base_default_expected, c: base_default_expected},
                  populate_from_object(default_directory, 'a', default_expected),
                  populate_from_object(default_directory, 'b', default_expected),
                  populate_from_object(default_directory, 'c', default_expected),
                  populate_from_object(user_directory, 'a', user_expected));
         }).then(assert_config_object);
      });

      it('environment variable overrides default value', function() {
         return Promise.join(tempdir(), tempdir()).then(function(args) {
            var default_directory = args[0];
            var user_directory = args[1];
            var base_expected = {a: 'a', b: 'b'};
            var expected = create_default_expected(base_expected);

            process.env = {a: 'c', b: 'd'};

            return Promise.join(
                  default_directory,
                  user_directory,
                  {a: process.env},
                  populate_from_object(default_directory, 'a', expected));
         }).then(assert_config_object);
      });

      it('environment variable overrides user value', function() {
         return Promise.join(tempdir(), tempdir()).then(function(args) {
            var default_directory = args[0];
            var user_directory = args[1];
            var base_default_expected = {a: 'a', b: 'b'};
            var default_expected = create_default_expected(base_default_expected);
            var user_expected = {a: 'c', b: 'd'};

            process.env = {a: 'e', b: 'f'};

            return Promise.join(
                  default_directory,
                  user_directory,
                  {a: process.env},
                  populate_from_object(default_directory, 'a', default_expected),
                  populate_from_object(user_directory, 'a', user_expected));
         }).then(assert_config_object);
      });
   });
});
