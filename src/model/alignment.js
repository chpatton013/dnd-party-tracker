'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

var ALIGNMENT_LAWFUL_GOOD = 'ALIGNMENT_LAWFUL_GOOD';
var ALIGNMENT_LAWFUL_NEUTRAL = 'ALIGNMENT_LAWFUL_NEUTRAL';
var ALIGNMENT_LAWFUL_EVIL = 'ALIGNMENT_LAWFUL_EVIL';
var ALIGNMENT_NEUTRAL_GOOD = 'ALIGNMENT_NEUTRAL_GOOD';
var ALIGNMENT_TRUE_NEUTRAL = 'ALIGNMENT_TRUE_NEUTRAL';
var ALIGNMENT_NEUTRAL_EVIL = 'ALIGNMENT_NEUTRAL_EVIL';
var ALIGNMENT_CHAOTIC_GOOD = 'ALIGNMENT_CHAOTIC_GOOD';
var ALIGNMENT_CHAOTIC_NEUTRAL = 'ALIGNMENT_CHAOTIC_NEUTRAL';
var ALIGNMENT_CHAOTIC_EVIL = 'ALIGNMENT_CHAOTIC_EVIL';
var ENUM_ALIGNMENT = [
   ALIGNMENT_LAWFUL_GOOD,
   ALIGNMENT_LAWFUL_NEUTRAL,
   ALIGNMENT_LAWFUL_EVIL,
   ALIGNMENT_NEUTRAL_GOOD,
   ALIGNMENT_TRUE_NEUTRAL,
   ALIGNMENT_NEUTRAL_EVIL,
   ALIGNMENT_CHAOTIC_GOOD,
   ALIGNMENT_CHAOTIC_NEUTRAL,
   ALIGNMENT_CHAOTIC_EVIL,
];

var MAX_ALIGNMENT_LENGTH = Utilities.max_length(ENUM_ALIGNMENT);

module.exports = function(sequelize, DataTypes) {
   return sequelize.define('alignment', {
      alignment_id: {
         type: DataTypes.BIGINT(11),
         field: 'alignment_id',
         allowNull: false,
         primaryKey: true,
         autoIncrement: true,
      },
      alignment: {
         type: DataTypes.STRING(MAX_ALIGNMENT_LENGTH),
         field: 'alignment',
         allowNull: false,
         validate: {
            isIn: [ENUM_ALIGNMENT],
         },
      },
   }, {
      classMethods: {
         getAlignmentLawfulGood: function() { return ALIGNMENT_LAWFUL_GOOD; },
         getAlignmentLawfulNeutral: function() { return ALIGNMENT_LAWFUL_NEUTRAL; },
         getAlignmentLawfulEvil: function() { return ALIGNMENT_LAWFUL_EVIL; },
         getAlignmentNeutralGood: function() { return ALIGNMENT_NEUTRAL_GOOD; },
         getAlignmentTrueNeutral: function() { return ALIGNMENT_TRUE_NEUTRAL; },
         getAlignmentNeutralEvil: function() { return ALIGNMENT_NEUTRAL_EVIL; },
         getAlignmentChaoticGood: function() { return ALIGNMENT_CHAOTIC_GOOD; },
         getAlignmentChaoticNeutral: function() { return ALIGNMENT_CHAOTIC_NEUTRAL; },
         getAlignmentChaoticEvil: function() { return ALIGNMENT_CHAOTIC_EVIL; },
         getEnumAlignment: function() { return ENUM_ALIGNMENT; },
         __seed: function() {
            return sequelize.models.alignment.bulkCreate([
               {alignment: ALIGNMENT_LAWFUL_GOOD},
               {alignment: ALIGNMENT_LAWFUL_NEUTRAL},
               {alignment: ALIGNMENT_LAWFUL_EVIL},
               {alignment: ALIGNMENT_NEUTRAL_GOOD},
               {alignment: ALIGNMENT_TRUE_NEUTRAL},
               {alignment: ALIGNMENT_NEUTRAL_EVIL},
               {alignment: ALIGNMENT_CHAOTIC_GOOD},
               {alignment: ALIGNMENT_CHAOTIC_NEUTRAL},
               {alignment: ALIGNMENT_CHAOTIC_EVIL},
            ]);
         },
      },
      indexes: [
         {fields: ['alignment'], unique: true},
      ],
      timestamps: false,
      paranoid: false,
      underscored: true,
   });
};
