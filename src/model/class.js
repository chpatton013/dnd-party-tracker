'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

var CLASS_BARBARIAN = 'CLASS_BARBARIAN';
var CLASS_BARD = 'CLASS_BARD';
var CLASS_CLERIC = 'CLASS_CLERIC';
var CLASS_DRUID = 'CLASS_DRUID';
var CLASS_FIGHTER = 'CLASS_FIGHTER';
var CLASS_MONK = 'CLASS_MONK';
var CLASS_PALADIN = 'CLASS_PALADIN';
var CLASS_RANGER = 'CLASS_RANGER';
var CLASS_ROGUE = 'CLASS_ROGUE';
var CLASS_SORCERER = 'CLASS_SORCERER';
var CLASS_WARLOCK = 'CLASS_WARLOCK';
var CLASS_WIZARD = 'CLASS_WIZARD';
var ENUM_CLASS = [
   CLASS_BARBARIAN,
   CLASS_BARD,
   CLASS_CLERIC,
   CLASS_DRUID,
   CLASS_FIGHTER,
   CLASS_MONK,
   CLASS_PALADIN,
   CLASS_RANGER,
   CLASS_ROGUE,
   CLASS_SORCERER,
   CLASS_WARLOCK,
   CLASS_WIZARD,
];

var MAX_CLASS_LENGTH = Utilities.max_length(ENUM_CLASS);

module.exports = function(sequelize, DataTypes) {
   return sequelize.define('class', {
      class_id: {
         type: DataTypes.BIGINT(11),
         field: 'class_id',
         allowNull: false,
         primaryKey: true,
         autoIncrement: true,
      },
      'class': {
         type: DataTypes.STRING(MAX_CLASS_LENGTH),
         field: 'class',
         allowNull: false,
         validate: {
            isIn: [ENUM_CLASS],
         },
      },
   }, {
      classMethods: {
         getClassBarbarian: function() { return CLASS_BARBARIAN; },
         getClassBard: function() { return CLASS_BARD; },
         getClassCleric: function() { return CLASS_CLERIC; },
         getClassDruid: function() { return CLASS_DRUID; },
         getClassFighter: function() { return CLASS_FIGHTER; },
         getClassMonk: function() { return CLASS_MONK; },
         getClassPaladin: function() { return CLASS_PALADIN; },
         getClassRanger: function() { return CLASS_RANGER; },
         getClassRogue: function() { return CLASS_ROGUE; },
         getClassSorcerer: function() { return CLASS_SORCERER; },
         getClassWarlock: function() { return CLASS_WARLOCK; },
         getClassWizard: function() { return CLASS_WIZARD; },
         getEnumclass: function() { return ENUM_CLASS; },
         __seed: function() {
            return sequelize.models.class.bulkCreate([
               {'class': CLASS_BARBARIAN},
               {'class': CLASS_BARD},
               {'class': CLASS_CLERIC},
               {'class': CLASS_DRUID},
               {'class': CLASS_FIGHTER},
               {'class': CLASS_MONK},
               {'class': CLASS_PALADIN},
               {'class': CLASS_RANGER},
               {'class': CLASS_ROGUE},
               {'class': CLASS_SORCERER},
               {'class': CLASS_WARLOCK},
               {'class': CLASS_WIZARD},
            ]);
         },
      },
      indexes: [
         {fields: ['class'], unique: true},
      ],
      timestamps: false,
      paranoid: false,
      underscored: true,
   });
};
