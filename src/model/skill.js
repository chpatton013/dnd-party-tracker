'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

var SKILL_ACROBATICS = 'SKILL_ACROBATICS';
var SKILL_ANIMAL_HANDLING = 'SKILL_ANIMAL_HANDLING';
var SKILL_ARCANA = 'SKILL_ARCANA';
var SKILL_ATHLETICS = 'SKILL_ATHLETICS';
var SKILL_DECEPTION = 'SKILL_DECEPTION';
var SKILL_HISTORY = 'SKILL_HISTORY';
var SKILL_INSIGHT = 'SKILL_INSIGHT';
var SKILL_INTIMIDATION = 'SKILL_INTIMIDATION';
var SKILL_INVESTIGATION = 'SKILL_INVESTIGATION';
var SKILL_MEDICINE = 'SKILL_MEDICINE';
var SKILL_NATURE = 'SKILL_NATURE';
var SKILL_PERCEPTION = 'SKILL_PERCEPTION';
var SKILL_PERFORMANCE = 'SKILL_PERFORMANCE';
var SKILL_PERSUASION = 'SKILL_PERSUASION';
var SKILL_RELIGION = 'SKILL_RELIGION';
var SKILL_SLEIGHT_OF_HAND = 'SKILL_SLEIGHT_OF_HAND';
var SKILL_STEALTH = 'SKILL_STEALTH';
var SKILL_SURVIVAL = 'SKILL_SURVIVAL';
var ENUM_SKILL = [
   SKILL_ACROBATICS,
   SKILL_ANIMAL_HANDLING,
   SKILL_ARCANA,
   SKILL_ATHLETICS,
   SKILL_DECEPTION,
   SKILL_HISTORY,
   SKILL_INSIGHT,
   SKILL_INTIMIDATION,
   SKILL_INVESTIGATION,
   SKILL_MEDICINE,
   SKILL_NATURE,
   SKILL_PERCEPTION,
   SKILL_PERFORMANCE,
   SKILL_PERSUASION,
   SKILL_RELIGION,
   SKILL_SLEIGHT_OF_HAND,
   SKILL_STEALTH,
   SKILL_SURVIVAL,
];

var MAX_SKILL_LENGTH = Utilities.max_length(ENUM_SKILL);

module.exports = function(sequelize, DataTypes) {
   return sequelize.define('skill', {
      skill_id: {
         type: DataTypes.BIGINT(11),
         field: 'skill_id',
         allowNull: false,
         primaryKey: true,
         autoIncrement: true,
      },
      name: {
         type: DataTypes.STRING(MAX_SKILL_LENGTH),
         field: 'name',
         allowNull: false,
         validate: {
            isIn: [ENUM_SKILL],
         },
      },
   }, {
      classMethods: {
         getSkillAcrobatics: function() { return SKILL_ACROBATICS; },
         getSkillAnimalHandling: function() { return SKILL_ANIMAL_HANDLING; },
         getSkillArcana: function() { return SKILL_ARCANA; },
         getSkillAthletics: function() { return SKILL_ATHLETICS; },
         getSkillDeception: function() { return SKILL_DECEPTION; },
         getSkillHistory: function() { return SKILL_HISTORY; },
         getSkillInsight: function() { return SKILL_INSIGHT; },
         getSkillIntimidation: function() { return SKILL_INTIMIDATION; },
         getSkillInvestigation: function() { return SKILL_INVESTIGATION; },
         getSkillMedicine: function() { return SKILL_MEDICINE; },
         getSkillNature: function() { return SKILL_NATURE; },
         getSkillPerception: function() { return SKILL_PERCEPTION; },
         getSkillPerformance: function() { return SKILL_PERFORMANCE; },
         getSkillPersuasion: function() { return SKILL_PERSUASION; },
         getSkillReligion: function() { return SKILL_RELIGION; },
         getSkillSleightOfHand: function() { return SKILL_SLEIGHT_OF_HAND; },
         getSkillStealth: function() { return SKILL_STEALTH; },
         getSkillSurvival: function() { return SKILL_SURVIVAL; },
         getEnumskill: function() { return ENUM_SKILL; },
         factory: function(options, ability) {
            return sequelize.models.skill.create(options).then(
                  function(skill) {
               return skill.setAbility(ability);
            });
         },
         __seed: function() {
            var Ability = sequelize.models.ability;

            var create = function(skill_name, ability_name) {
               Ability.find({where: {name: ability_name}}).then(
                     function(ability) {
                  return sequelize.models.skill.factory(ability, {
                     name: skill_name,
                  });
               });
            };

            return Promise.join(
                  create(SKILL_ACROBATICS, Ability.getAbilityDexterity()),
                  create(SKILL_ANIMAL_HANDLING, Ability.getAbilityWisdom()),
                  create(SKILL_ARCANA, Ability.getAbilityIntelligence()),
                  create(SKILL_ATHLETICS, Ability.getAbilityStrength()),
                  create(SKILL_DECEPTION, Ability.getAbilityCharisma()),
                  create(SKILL_HISTORY, Ability.getAbilityIntelligence()),
                  create(SKILL_INSIGHT, Ability.getAbilityWisdom()),
                  create(SKILL_INTIMIDATION, Ability.getAbilityCharisma()),
                  create(SKILL_INVESTIGATION, Ability.getAbilityIntelligence()),
                  create(SKILL_MEDICINE, Ability.getAbilityWisdom()),
                  create(SKILL_NATURE, Ability.getAbilityIntelligence()),
                  create(SKILL_PERCEPTION, Ability.getAbilityWisdom()),
                  create(SKILL_PERFORMANCE, Ability.getAbilityCharisma()),
                  create(SKILL_PERSUASION, Ability.getAbilityCharisma()),
                  create(SKILL_RELIGION, Ability.getAbilityIntelligence()),
                  create(SKILL_SLEIGHT_OF_HAND, Ability.getAbilityDexterity()),
                  create(SKILL_STEALTH, Ability.getAbilityDexterity()),
                  create(SKILL_SURVIVAL, Ability.getAbilityWisdom()));
         },
      },
      indexes: [
         {fields: ['name'], unique: true},
      ],
      timestamps: false,
      paranoid: false,
      underscored: true,
   });
};
