'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

module.exports = function(sequelize, DataTypes) {
   return sequelize.define('character_skill', {
      character_skill_id: {
         type: DataTypes.BIGINT(11),
         field: 'character_skill_id',
         allowNull: false,
         primaryKey: true,
         autoIncrement: true,
      },
      proficiency: {
         type: DataTypes.DECIMAL(10, 2),
         field: 'proficiency',
         allowNull: false,
         validate: {
            min: 0.0,
         },
      },
      advantage: {
         type: DataTypes.INTEGER(11),
         field: 'advantage',
         allowNull: false,
      },
   }, {
      timestamps: false,
      paranoid: false,
      underscored: true,
   });
};
