'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

var RACE_DRAGONBORN = 'RACE_DRAGONBORN';
var RACE_DWARF = 'RACE_DWARF';
var RACE_ELF = 'RACE_ELF';
var RACE_GNOME = 'RACE_GNOME';
var RACE_HALFLING = 'RACE_HALFLING';
var RACE_HALF_ELF = 'RACE_HALF_ELF';
var RACE_HALF_ORC = 'RACE_HALF_ORC';
var RACE_HUMAN = 'RACE_HUMAN';
var RACE_TIEFLING = 'RACE_TIEFLING';
var ENUM_RACE = [
   RACE_DRAGONBORN,
   RACE_DWARF,
   RACE_ELF,
   RACE_GNOME,
   RACE_HALFLING,
   RACE_HALF_ELF,
   RACE_HALF_ORC,
   RACE_HUMAN,
   RACE_TIEFLING,
];

var MAX_RACE_LENGTH = Utilities.max_length(ENUM_RACE);

var SIZE_SMALL = 'SIZE_SMALL';
var SIZE_MEDIUM = 'SIZE_MEDIUM';
var ENUM_SIZE = [
   SIZE_SMALL,
   SIZE_MEDIUM,
];

var MAX_SIZE_LENGTH = Utilities.max_length(ENUM_SIZE);

module.exports = function(sequelize, DataTypes) {
   return sequelize.define('race', {
      race_id: {
         type: DataTypes.BIGINT(11),
         field: 'race_id',
         allowNull: false,
         primaryKey: true,
         autoIncrement: true,
      },
      race: {
         type: DataTypes.STRING(MAX_RACE_LENGTH),
         field: 'race',
         allowNull: false,
         validate: {
            isIn: [ENUM_RACE],
         },
      },
      size: {
         type: DataTypes.STRING(MAX_SIZE_LENGTH),
         field: 'size',
         allowNull: false,
         validate: {
            isIn: [ENUM_SIZE],
         },
      },
      speed: {
         type: DataTypes.INTEGER(11),
         field: 'speed',
         allowNull: false,
         validate: {
            min: 0,
         },
      },
   }, {
      classMethods: {
         getRaceDragonborn: function() { return RACE_DRAGONBORN; },
         getRaceDwarf: function() { return RACE_DWARF; },
         getRaceElf: function() { return RACE_ELF; },
         getRaceGnome: function() { return RACE_GNOME; },
         getRaceHalfling: function() { return RACE_HALFLING; },
         getRaceHalfElf: function() { return RACE_HALF_ELF; },
         getRaceHalfOrc: function() { return RACE_HALF_ORC; },
         getRaceHuman: function() { return RACE_HUMAN; },
         getRaceTiefling: function() { return RACE_TIEFLING; },
         getEnumRace: function() { return ENUM_RACE; },
         getSizeSmall: function() { return SIZE_SMALL; },
         getSizeMedium: function() { return SIZE_MEDIUM; },
         getEnumSize: function() { return ENUM_SIZE; },
         __seed: function() {
            return sequelize.models.race.bulkCreate([
               {race: RACE_DRAGONBORN, size: SIZE_MEDIUM, speed: 30},
               {race: RACE_DWARF, size: SIZE_MEDIUM, speed: 25},
               {race: RACE_ELF, size: SIZE_MEDIUM, speed: 30},
               {race: RACE_GNOME, size: SIZE_SMALL, speed: 25},
               {race: RACE_HALFLING, size: SIZE_SMALL, speed: 25},
               {race: RACE_HALF_ELF, size: SIZE_MEDIUM, speed: 30},
               {race: RACE_HALF_ORC, size: SIZE_MEDIUM, speed: 30},
               {race: RACE_HUMAN, size: SIZE_MEDIUM, speed: 30},
               {race: RACE_TIEFLING, size: SIZE_MEDIUM, speed: 30},
            ]);
         },
      },
      indexes: [
         {fields: ['race'], unique: true},
         {fields: ['size']},
      ],
      timestamps: false,
      paranoid: false,
      underscored: true,
   });
};
