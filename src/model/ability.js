'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

var ABILITY_CHARISMA = 'ABILITY_CHARISMA';
var ABILITY_CONSTITUTION = 'ABILITY_CONSTITUTION';
var ABILITY_DEXTERITY = 'ABILITY_DEXTERITY';
var ABILITY_INTELLIGENCE = 'ABILITY_INTELLIGENCE';
var ABILITY_STRENGTH = 'ABILITY_STRENGTH';
var ABILITY_WISDOM = 'ABILITY_WISDOM';
var ENUM_ABILITY = [
   ABILITY_CHARISMA,
   ABILITY_CONSTITUTION,
   ABILITY_DEXTERITY,
   ABILITY_INTELLIGENCE,
   ABILITY_STRENGTH,
   ABILITY_WISDOM,
];

var MAX_ABILITY_LENGTH = Utilities.max_length(ENUM_ABILITY);

module.exports = function(sequelize, DataTypes) {
   return sequelize.define('ability', {
      ability_id: {
         type: DataTypes.BIGINT(11),
         field: 'ability_id',
         allowNull: false,
         primaryKey: true,
         autoIncrement: true,
      },
      name: {
         type: DataTypes.STRING(MAX_ABILITY_LENGTH),
         field: 'name',
         allowNull: false,
         validate: {
            isIn: [ENUM_ABILITY],
         },
      },
   }, {
      classMethods: {
         getAbilityCharisma: function() { return ABILITY_CHARISMA; },
         getAbilityConstitution: function() { return ABILITY_CONSTITUTION; },
         getAbilityDexterity: function() { return ABILITY_DEXTERITY; },
         getAbilityIntelligence: function() { return ABILITY_INTELLIGENCE; },
         getAbilityStrength: function() { return ABILITY_STRENGTH; },
         getAbilityWisdom: function() { return ABILITY_WISDOM; },
         getEnumAbility: function() { return ENUM_ABILITY; },
         __seed: function() {
            return sequelize.models.ability.bulkCreate([
               {name: ABILITY_CHARISMA},
               {name: ABILITY_CONSTITUTION},
               {name: ABILITY_DEXTERITY},
               {name: ABILITY_INTELLIGENCE},
               {name: ABILITY_STRENGTH},
               {name: ABILITY_WISDOM},
            ]);
         },
      },
      indexes: [
         {fields: ['name'], unique: true},
      ],
      timestamps: false,
      paranoid: false,
      underscored: true,
   });
};
