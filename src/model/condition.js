'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

var CONDITION_BLINDED = 'CONDITION_BLINDED';
var CONDITION_CHARMED = 'CONDITION_CHARMED';
var CONDITION_DEAFENED = 'CONDITION_DEAFENED';
var CONDITION_FRIGHTENED = 'CONDITION_FRIGHTENED';
var CONDITION_GRAPPLED = 'CONDITION_GRAPPLED';
var CONDITION_INCAPACITATED = 'CONDITION_INCAPACITATED';
var CONDITION_INVISIBLE = 'CONDITION_INVISIBLE';
var CONDITION_PARALYZED = 'CONDITION_PARALYZED';
var CONDITION_PETRIFIED = 'CONDITION_PETRIFIED';
var CONDITION_POISONED = 'CONDITION_POISONED';
var CONDITION_RESTRAINED = 'CONDITION_RESTRAINED';
var CONDITION_STUNNED = 'CONDITION_STUNNED';
var ENUM_CONDITION = [
   CONDITION_BLINDED,
   CONDITION_CHARMED,
   CONDITION_DEAFENED,
   CONDITION_FRIGHTENED,
   CONDITION_GRAPPLED,
   CONDITION_INCAPACITATED,
   CONDITION_INVISIBLE,
   CONDITION_PARALYZED,
   CONDITION_PETRIFIED,
   CONDITION_POISONED,
   CONDITION_RESTRAINED,
   CONDITION_STUNNED,
];

var MAX_CONDITION_LENGTH = Utilities.max_length(ENUM_CONDITION);

module.exports = function(sequelize, DataTypes) {
   return sequelize.define('condition', {
      condition_id: {
         type: DataTypes.BIGINT(11),
         field: 'condition_id',
         allowNull: false,
         primaryKey: true,
         autoIncrement: true,
      },
      condition: {
         type: DataTypes.STRING(MAX_CONDITION_LENGTH),
         field: 'condition',
         allowNull: false,
         validate: {
            isIn: [ENUM_CONDITION],
         },
      },
   }, {
      classMethods: {
         getConditionBlinded: function() { return CONDITION_BLINDED; },
         getConditionCharmed: function() { return CONDITION_CHARMED; },
         getConditionDeafened: function() { return CONDITION_DEAFENED; },
         getConditionFrightened: function() { return CONDITION_FRIGHTENED; },
         getConditionGrappled: function() { return CONDITION_GRAPPLED; },
         getConditionIncapacitated: function() { return CONDITION_INCAPACITATED; },
         getConditionInvisible: function() { return CONDITION_INVISIBLE; },
         getConditionParalyzed: function() { return CONDITION_PARALYZED; },
         getConditionPetrified: function() { return CONDITION_PETRIFIED; },
         getConditionPoisoned: function() { return CONDITION_POISONED; },
         getConditionRestrained: function() { return CONDITION_RESTRAINED; },
         getConditionStunned: function() { return CONDITION_STUNNED; },
         getEnumCondition: function() { return ENUM_CONDITION; },
         __seed: function() {
            return sequelize.models.condition.bulkCreate([
               {condition: CONDITION_BLINDED},
               {condition: CONDITION_CHARMED},
               {condition: CONDITION_DEAFENED},
               {condition: CONDITION_FRIGHTENED},
               {condition: CONDITION_GRAPPLED},
               {condition: CONDITION_INCAPACITATED},
               {condition: CONDITION_INVISIBLE},
               {condition: CONDITION_PARALYZED},
               {condition: CONDITION_PETRIFIED},
               {condition: CONDITION_POISONED},
               {condition: CONDITION_RESTRAINED},
               {condition: CONDITION_STUNNED},
            ]);
         },
      },
      indexes: [
         {fields: ['condition'], unique: true},
      ],
      timestamps: false,
      paranoid: false,
      underscored: true,
   });
};
