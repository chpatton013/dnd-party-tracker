'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

var MIN_NAME_LENGTH = 1;
var MAX_NAME_LENGTH = 255;

module.exports = function(sequelize, DataTypes) {
   return sequelize.define('character', {
      character_id: {
         type: DataTypes.BIGINT(11),
         field: 'character_id',
         allowNull: false,
         primaryKey: true,
         autoIncrement: true,
      },
      name: {
         type: DataTypes.STRING(MAX_NAME_LENGTH),
         field: 'name',
         allowNull: false,
         validate: {
            type: function(value) {
               if (typeof value !== 'string') {
                  throw new TypeError('Invalid name: Only strings are allowed!');
               }
            },
            len: [MIN_NAME_LENGTH, MAX_NAME_LENGTH],
         },
      },
      experience_points: {
         type: DataTypes.INTEGER(11),
         field: 'experience_points',
         allowNull: false,
         validate: {
            min: 0,
         },
      },
      current_hit_points: {
         type: DataTypes.INTEGER(11),
         field: 'current_hit_points',
         allowNull: false,
         validate: {
            min: 0,
         },
      },
      maximum_hit_points: {
         type: DataTypes.INTEGER(11),
         field: 'maximum_hit_points',
         allowNull: false,
         validate: {
            min: 0,
         },
      },
      death_save_successes: {
         type: DataTypes.INTEGER(11),
         field: 'death_save_successes',
         allowNull: false,
         validate: {
            min: 0,
            max: 3,
         },
      },
      death_save_failures: {
         type: DataTypes.INTEGER(11),
         field: 'death_save_failures',
         allowNull: false,
         validate: {
            min: 0,
            max: 3,
         },
      },
      armor_class: {
         type: DataTypes.INTEGER(11),
         field: 'armor_class',
         allowNull: false,
         validate: {
            min: 1,
         },
      },
      initiative_modifier: {
         type: DataTypes.INTEGER(11),
         field: 'initiative_modifier',
         allowNull: false,
         validate: {
            min: 1,
         },
      },
      speed_modifier: {
         type: DataTypes.INTEGER(11),
         field: 'speed_modifier',
         allowNull: false,
         validate: {
            isInt: true,
         },
      },
   }, {
      indexes: [
         {fields: ['name']},
         {fields: ['maximum_hit_points', 'current_hit_points']},
         {fields: ['death_save_failures', 'death_save_successes']},
      ],
      timestamps: false,
      paranoid: false,
      underscored: true,
   });
};
