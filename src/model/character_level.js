'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

module.exports = function(sequelize, DataTypes) {
   return sequelize.define('character_level', {
      character_level_id: {
         type: DataTypes.BIGINT(11),
         field: 'character_level_id',
         allowNull: false,
         primaryKey: true,
         autoIncrement: true,
      },
      level: {
         type: DataTypes.INTEGER(11),
         field: 'level',
         allowNull: false,
         validate: {
            min: 1,
            max: 20,
         },
      },
   }, {
      indexes: [
         {fields: ['level']},
      ],
      timestamps: false,
      paranoid: false,
      underscored: true,
   });
};
