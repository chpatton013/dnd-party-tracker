'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

module.exports = function(sequelize, DataTypes) {
   return sequelize.define('character_ability', {
      character_ability_id: {
         type: DataTypes.BIGINT(11),
         field: 'character_ability_id',
         allowNull: false,
         primaryKey: true,
         autoIncrement: true,
      },
      score: {
         type: DataTypes.INTEGER(11),
         field: 'score',
         allowNull: false,
         validate: {
            min: 1,
            max: 20,
         },
      },
      skill_proficiency: {
         type: DataTypes.DECIMAL(10, 2),
         field: 'skill_proficiency',
         allowNull: false,
         validate: {
            min: 0.0,
         },
      },
      skill_advantage: {
         type: DataTypes.INTEGER(11),
         field: 'skill_advantage',
         allowNull: false,
      },
      save_proficiency: {
         type: DataTypes.DECIMAL(10, 2),
         field: 'save_proficiency',
         allowNull: false,
         validate: {
            min: 0.0,
         },
      },
      save_advantage: {
         type: DataTypes.INTEGER(11),
         field: 'save_advantage',
         allowNull: false,
      },
   }, {
      timestamps: false,
      paranoid: false,
      underscored: true,
   });
};
