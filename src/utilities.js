'use strict';

var _ = require('lodash');
var path = require('path');
var Promise = require('bluebird');

module.exports = {
   project_path: project_path,
   respond: respond,
   max_length: max_length,
   to_int: to_int,
   unique: unique,
   merge: merge,
   filter: filter,
   lookup: lookup,
   repeat: repeat,
   exclusive_min: exclusive_min,
   type_string: type_string,
   validation: {
      exception: validation_exception,
      is_string: validation_is_string,
      is_int: validation_is_int,
      is_float: validation_is_float,
      is_positive: validation_is_positive,
      is_not_negative: validation_is_not_negative,
      is_primary_key: validation_is_primary_key,
      is_less_than: validation_is_less_than,
      is_not_less_than: validation_is_not_less_than,
      is_greater_than: validation_is_greater_than,
      is_not_greater_than: validation_is_not_greater_than,
      length: validation_length,
      fields: validation_fields,
      allowed: validation_allowed,
   },
   json: {
      ability: ability_to_json,
      single_ability: single_ability_to_json,
      skill: skill_to_json,
      single_skill: single_skill_to_json,
   },
};

function ability_to_json(ability) {
   return {
      ability_id: ability.ability_id,
      ability_name: ability.name,
   };
}

function single_ability_to_json(ability) {
   var json = ability_to_json(ability);
   return Promise.map(ability.getSkills(), function(skill) {
      return skill_to_json(skill);
   }).then(function(skills) {
      json.skills = skills;
      return json;
   });
}

function skill_to_json(skill) {
   return {
      skill_id: skill.skill_id,
      skill_name: skill.skill,
      ability_id: skill.ability_id,
   };
}

function single_skill_to_json(skill) {
   return skill.getAbility().then(function(ability) {
      return {
         skill_id: skill.skill_id,
         skill_name: skill.skill,
         ability: ability_to_json(ability),
      };
   });
}

// Get the absolute path for a file relative to the project root.
function project_path(filename) {
   var project_root = path.dirname(__dirname);
   return path.resolve(project_root, filename);
}

function respond(response, code, get_data) {
   return Promise.resolve(get_data).then(function(content) {
      return response.status(code).json(content);
   }).catch(function(e) {
      var status_code = e.status_code || 500;
      var content = e.message || 'Internal server error';
      return response.status(status_code).send(content);
   });
}

function max_length(list) {
   return _.max(_.map(list, function(x) {
      return x.length;
   }));
}

function to_int(i) {
   return parseInt(i, 10);
}

function unique(list) {
   var lookup = {};
   var set = [];
   list.forEach(function(item) {
      if (!lookup[item]) {
         lookup[item] = true;
         set.push(item);
      }
   });
   return set;
}

function merge(a, b) {
   var c = {};

   for (var i in a) {
      c[i] = a[i];
   }

   for (var i in b) {
      c[i] = b[i];
   }

   return c;
}

function filter(list, filter, field) {
   var index = lookup(filter, field);

   return list.filter(function(member) {
      return member[field] in index;
   });
}

function lookup(list, field) {
   var lookup = {};
   list.forEach(function(member, index) {
      lookup[member[field]] = index;
   });
   return lookup;
}

function repeat(x, n) {
   var s = ''; 
   for (var i = 0; i < n; ++i) {
      s += x;
   }
   return s; 
}

function exclusive_min(min, name) {
   return function(value) {
      if (value <= min) {
         throw new RangeError('Invalid ' + name + ': must be greater than 0!');
      }
   };
}

function type_string(name) {
   return function(value) {
      if (typeof value !== 'string') {
         throw new TypeError('Invalid ' + name + ': must be a string!');
      }
   };
}

function validation_exception(message) {
   return new function() {
      this.status_code = 422;
      this.message = message;
   }();
}

function validation_is_string(s, message) {
   if (typeof s !== 'string') {
      throw validation_exception(message);
   }
}

function validation_is_int(i, message) {
   if (isNaN(to_int(i))) {
      throw validation_exception(message);
   }
}

function validation_is_float(i, message) {
   if (isNaN(parseFloat(i))) {
      throw validation_exception(message);
   }
}

function validation_is_positive(i, message) {
   if (i <= 0) {
      throw validation_exception(message);
   }
}

function validation_is_not_negative(i, message) {
   if (i < 0) {
      throw validation_exception(message);
   }
}

function validation_is_primary_key(i, message) {
   validation_is_int(i);
   validation_is_positive(i);
}

function validation_is_less_than(a, b, message) {
   if (!(a < b)) {
      throw validation_exception(message);
   }
}

function validation_is_not_less_than(a, b, message) {
   if (a < b) {
      throw validation_exception(message);
   }
}

function validation_is_greater_than(a, b, message) {
   if (!(a > b)) {
      throw validation_exception(message);
   }
}

function validation_is_not_greater_than(a, b, message) {
   if (a > b) {
      throw validation_exception(message);
   }
}

function validation_length(s, min, max, message) {
   if (min !== undefined && s.length < min) {
      throw validation_exception(message);
   }
   if (max !== undefined && s.length > max) {
      throw validation_exception(message);
   }
}

function validation_fields(o, required, optional) {
   var allowed = (required || []).concat(optional || []);
   var allowed_lookup = {}
   allowed.forEach(function(field) {
      allowed_lookup[field] = true;
   });

   var missing = [];
   for (var field in (required || [])) {
      if (!o.hasOwnProperty(field)) {
         missing.push(field);
      }
   }
   if (missing.length > 0) {
      throw validation_exception('Missing fields: ' + missing.join(', ') + '.');
   }

   var extra = [];
   for (var prop in o) {
      if (o.hasOwnProperty(prop) && !allowed_lookup[prop]) {
         extra.push(field);
      }
   }
   if (missing.length > 0) {
      throw validation_exception('Extra fields: ' + missing.join(', ') + '.');
   }
}

function validation_allowed(allowed, element) {
   if (allowed.indexOf(element) < 0) {
      throw validation_exception('Element not in allowed set.');
   }
}
