'use strict';

var path = require('path');

module.exports = function(model_directory, sequelize) {
   var model_import = function(model_directory, sequelize) {
      var relative_model_directory = path.relative(__dirname, model_directory);
      return function(model) {
         var relative_model_file = path.join(relative_model_directory, model);
         return sequelize.import('./' + relative_model_file);
      };
   }(model_directory, sequelize);

   var Ability = model_import('ability');
   var Alignment = model_import('alignment');
   var Character = model_import('character');
   var CharacterAbility = model_import('character_ability');
   var CharacterLevel = model_import('character_level');
   var CharacterSkill = model_import('character_skill');
   var Class = model_import('class');
   var Condition = model_import('condition');
   var Race = model_import('race');
   var Skill = model_import('skill');

   Character.belongsTo(Alignment, {
      as: 'Alignment',
      foreignKey: 'alignment_id',
   });
   Character.belongsTo(Race, {
      as: 'Race',
      foreignKey: 'race_id',
   });
   Character.hasMany(CharacterAbility, {
      as: 'CharacterAbilities',
      foreignKey: 'character_id',
   });
   Character.hasMany(CharacterLevel, {
      as: 'CharacterLevels',
      foreignKey: 'character_id',
   });
   Character.hasMany(CharacterSkill, {
      as: 'CharacterSkills',
      foreignKey: 'character_id',
   });

   CharacterAbility.belongsTo(Ability, {
      as: 'Ability',
      foreignKey: 'ability_id',
   });

   CharacterLevel.belongsTo(Class, {
      as: 'Class',
      foreignKey: 'class_id',
   });

   CharacterSkill.belongsTo(Skill, {
      as: 'Skill',
      foreignKey: 'skill_id',
   });

   Condition.belongsToMany(Character, {
      through: 'character_conditions',
      as: 'Characters',
      foreignKey: 'condition_id',
      otherKey: 'character_id',
   });

   Ability.hasMany(Skill, {
      as: 'Skill',
      foreignKey: 'ability_id',
   });
   Skill.belongsTo(Ability, {
      as: 'Ability',
      foreignKey: 'ability_id',
   });

   return new function() {
      this.Ability = Ability;
      this.Alignment = Alignment;
      this.Character = Character;
      this.CharacterAbility = CharacterAbility;
      this.CharacterLevel = CharacterLevel;
      this.CharacterSkill = CharacterSkill;
      this.Class = Class;
      this.Condition = Condition;
      this.Race = Race;
      this.Skill = Skill;
   }();
};
