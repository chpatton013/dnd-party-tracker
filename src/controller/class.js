'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

module.exports = function() {
   return new function() {
      this.validation = {
      };

      this.methods = {
         get_classes: function() {
         },
         get_class: function(class_id) {
         },
         create_class: function(class_name) {
         },
         modify_class: function(class_id, class_name) {
         },
      };

      this.routes = {
         get_classes: {
            method: 'get',
            path: '/classes',
            responder: function(request, response) {
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_classes());
            }.bind(this),
         },
         get_class: {
            method: 'get',
            path: '/class/:class_id',
            responder: function(request, response) {
               var class_id = request.param('class_id');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_class(class_id));
            }.bind(this),
         },
         create_class: {
            method: 'post',
            path: '/class',
            responder: function(request, response) {
               var class_name = request.param('class_name');
               return Utilities.respond(
                     response,
                     201,
                     this.methods.create_class(class_name));
            }.bind(this),
         },
         modify_class: {
            method: 'patch',
            path: '/class/:class_id',
            responder: function(request, response) {
               var class_id = request.param('class_id');
               var class_name = request.param('class_name');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.modify_class(class_id, class_name));
            }.bind(this),
         },
      };
   }();
};
