'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

module.exports = function(AbilityModel) {
   return new function() {
      this.validation = {
         ability_id: function(ability_id) {
            return AbilityModel.findById(ability_id);
         },
         ability_name: function(ability_name) {
            // TODO: implement me
         },
      };

      this.methods = {
         get_abilities: function() {
            return AbilityModel.findAll().then(function(abilities) {
               return {
                  abilities: abilities.map(Utilities.json.ability),
               };
            });
         },
         get_ability: function(ability_id) {
            return this.validation.ability_id(ability_id)
               .then(AbilityModel.findById(ability_id))
               .then(Utilities.json.single_ability);
         },
         create_ability: function(ability_name) {
            return Promise.method(function() {
               this.validation.ability_name(ability_name);
            }.bind(this))().then(function() {
               return AbilityModel.create({ability: ability_name});
            }).then(Utilities.json.single_ability);
         },
         modify_ability: function(ability_id, ability_name) {
            return this.validation.ability_id(ability_id).then(function() {
               ability_name && this.validation.ability_name(ability_name);
            }.bind(this)).then(function() {
               var attributes = {};
               if (ability_name) {
                  attributes.ability_name = ability_name;
               }

               if (Object.keys(attributes).length === 0) {
                  return ability;
               } else {
                  ability.update(attributes);
               }
            }).then(Utilities.json.single_ability);
         },
      };

      this.routes = {
         get_abilities: {
            method: 'get',
            path: '/abilities',
            responder: function(request, response) {
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_abilities());
            }.bind(this),
         },
         get_ability: {
            method: 'get',
            path: '/ability/:ability_id',
            responder: function(request, response) {
               var ability_id = request.param('ability_id');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_ability(ability_id));
            }.bind(this),
         },
         create_ability: {
            method: 'post',
            path: '/ability',
            responder: function(request, response) {
               var ability_name = request.param('ability_name');
               return Utilities.respond(
                     response,
                     201,
                     this.methods.create_ability(ability_name));
            }.bind(this),
         },
         modify_ability: {
            method: 'patch',
            path: '/ability/:ability_id',
            responder: function(request, response) {
               var ability_id = request.param('ability_id');
               var ability_name = request.param('ability_name');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.modify_ability(ability_id, ability_name));
            }.bind(this),
         },
      };
   }();
};
