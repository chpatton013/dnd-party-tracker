'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

module.exports = function() {
   return new function() {
      this.validation = {
      };

      this.methods = {
         get_races: function() {
         },
         get_race: function(race_id) {
         },
         create_race: function(race_name) {
         },
         modify_race: function(race_id, race_name) {
         },
      };

      this.routes = {
         get_races: {
            method: 'get',
            path: '/races',
            responder: function(request, response) {
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_races());
            }.bind(this),
         },
         get_race: {
            method: 'get',
            path: '/race/:race_id',
            responder: function(request, response) {
               var race_id = request.param('race_id');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_race(race_id));
            }.bind(this),
         },
         create_race: {
            method: 'post',
            path: '/race',
            responder: function(request, response) {
               var race_name = request.param('race_name');
               return Utilities.respond(
                     response,
                     201,
                     this.methods.create_race(race_name));
            }.bind(this),
         },
         modify_race: {
            method: 'patch',
            path: '/race/:race_id',
            responder: function(request, response) {
               var race_id = request.param('race_id');
               var race_name = request.param('race_name');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.modify_race(race_id, race_name));
            }.bind(this),
         },
      };
   }();
};
