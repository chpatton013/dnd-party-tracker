'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

module.exports = function() {
   return new function() {
      this.validation = {
      };

      this.methods = {
         get_alignments: function() {
         },
         get_alignment: function(alignment_id) {
         },
         create_alignment: function(alignment_name) {
         },
         modify_alignment: function(alignment_id, alignment_name) {
         },
      };

      this.routes = {
         get_alignments: {
            method: 'get',
            path: '/alignments',
            responder: function(request, response) {
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_alignments());
            }.bind(this),
         },
         get_alignment: {
            method: 'get',
            path: '/alignment/:alignment_id',
            responder: function(request, response) {
               var alignment_id = request.param('alignment_id');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_alignment(alignment_id));
            }.bind(this),
         },
         create_alignment: {
            method: 'post',
            path: '/alignment',
            responder: function(request, response) {
               var alignment_name = request.param('alignment_name');
               return Utilities.respond(
                     response,
                     201,
                     this.methods.create_alignment(alignment_name));
            }.bind(this),
         },
         modify_alignment: {
            method: 'patch',
            path: '/alignment/:alignment_id',
            responder: function(request, response) {
               var alignment_id = request.param('alignment_id');
               var alignment_name = request.param('alignment_name');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.modify_alignment(alignment_id, alignment_name));
            }.bind(this),
         },
      };
   }();
};
