'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

module.exports = function(SkillModel, AbilityModel, AbilityController) {
   return new function() {
      this.validation = {
         skill_id: function(skill_id) {
            return SkillModel.findById(skill_id);
         },
         skill_name: function(skill_name) {
            // TODO: implement me
         },
         ability_id: function(ability_id) {
            return AbilityController.validation.ability_id(ability_id);
         },
      };

      this.methods = {
         get_skills: function() {
            return Promise.join(
                  SkillModel.findAll(),
                  AbilityController.methods.get_abilities()).then(
                     function(args) {
               var skills = args[0];
               var abilities = args[1];

               var skill_id_lookup = Utilities.lookup(
                     skills.skills, 'skill_id');

               abilities.ability_id_lookup = Utilities.lookup(
                     abilities.abilities, 'ability_id');

               return Utilities.merge(abilities, {
                  skills: skills.map(Utilities.skill_to_json),
                  skill_id_lookup: skill_id_lookup,
               };
            });
         },
         get_skill: function(skill_id) {
            return Promise.method(function() {
               this.validation.skill_id(skill_id);
               return SkillModel.findById(skill_id);
            }.bind(this))().then(Utilities.single_skill_to_json);
         },
         create_skill: function(skill_name, ability_id) {
            return this.validation.ability_id(ability_id).then(function() {
               this.validation.skill_name(skill_name);
            }.bind(this)).then(Ability.findById(ability_id).then(
                  function(ability) {
               return SkillModel.factory(ability, {
                  name: skill_name,
               });
            }.bind(this)).then(Utilities.single_skill_to_json);
         },
         modify_skill: function(skill_id, skill_name, ability_id) {
            return Promise.try(function() {
               skill_name && this.validation.skill_name(skill_name);
               if (ability_id) {
                  this.validation.ability_id(ability_id);
               }
            }.bind(this)).then(function() {
               return SkillModel.findById(skill_id);
            }).then(function(skill) {
               var attributes = {};
               if (skill_name) {
                  attributes.name = skill_name;
               }
               if (ability_id) {
                  attributes.ability_id = ability_id;
               }

               if (Object.keys(attributes).length === 0) {
                  return skill;
               } else {
                  skill.update(attributes);
               }
            }.bind(this)).then(Utilities.single_skill_to_json);
         },
      };

      this.routes = {
         get_skills: {
            method: 'get',
            path: '/skills',
            responder: function(request, response) {
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_skills());
            }.bind(this),
         },
         get_skill: {
            method: 'get',
            path: '/skill/:skill_id',
            responder: function(request, response) {
               var skill_id = request.param('skill_id');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_skill(skill_id));
            }.bind(this),
         },
         create_skill: {
            method: 'post',
            path: '/skill',
            responder: function(request, response) {
               var skill_name = request.param('skill_name');
               var ability_id = request.param('ability_id');
               return Utilities.respond(
                     response,
                     201,
                     this.methods.create_skill(skill_name, ability_id));
            }.bind(this),
         },
         modify_skill: {
            method: 'patch',
            path: '/skill/:skill_id',
            responder: function(request, response) {
               var skill_id = request.param('skill_id');
               var skill_name = request.param('skill_name');
               var ability_id = request.param('ability_id');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.modify_skill(
                           skill_id,
                           skill_name,
                           ability_id));
            }.bind(this),
         },
      };
   }();
};
