'use strict';

var Promise = require('bluebird');
var Utilities = require('../utilities');

module.exports = function() {
   return new function() {
      this.validation = {
      };

      this.methods = {
         get_conditions: function() {
         },
         get_condition: function(condition_id) {
         },
         create_condition: function(condition_name) {
         },
         modify_condition: function(condition_id, condition_name) {
         },
      };

      this.routes = {
         get_conditions: {
            method: 'get',
            path: '/conditions',
            responder: function(request, response) {
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_conditions());
            }.bind(this),
         },
         get_condition: {
            method: 'get',
            path: '/condition/:condition_id',
            responder: function(request, response) {
               var condition_id = request.param('condition_id');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.get_condition(condition_id));
            }.bind(this),
         },
         create_condition: {
            method: 'post',
            path: '/condition',
            responder: function(request, response) {
               var condition_name = request.param('condition_name');
               return Utilities.respond(
                     response,
                     201,
                     this.methods.create_condition(condition_name));
            }.bind(this),
         },
         modify_condition: {
            method: 'patch',
            path: '/condition/:condition_id',
            responder: function(request, response) {
               var condition_id = request.param('condition_id');
               var condition_name = request.param('condition_name');
               return Utilities.respond(
                     response,
                     200,
                     this.methods.modify_condition(condition_id, condition_name));
            }.bind(this),
         },
      };
   }();
};
