'use strict';

var path = require('path');
var fs = require('fs-promise');

var file_extension = '.json';

module.exports = function(default_config_directory, user_config_directory) {
   function get_user_config(name) {
      try {
         return get_config(user_config_directory, name);
      } catch (e) {
         return {};
      }
   }

   function get_default_config(name) {
      return get_config(default_config_directory, name);
   }

   function resolve_config(files) {
      var config = {};
      for (var i in files) {
         // Remove trailing extension to get name. (eg: a.json -> a)
         var name = path.basename(files[i], file_extension);
         var default_config = get_default_config(name);
         var user_config = get_user_config(name);
         config[name] = resolve_values(default_config, user_config);
      }
      return config;
   }

   return fs.walk(default_config_directory).then(function(contents) {
      return contents.filter(function(entry) {
         return entry.stats.isFile() && entry.path.endsWith(file_extension);
      }).map(function(entry) {
         return entry.path;
      });
   }).then(resolve_config);
}

function get_config(directory, name) {
   if (!directory) {
      return {};
   } else {
      var filename = path.join(directory, name + file_extension);
      return require(path.relative(__dirname, filename));
   }
}

function resolve_value(default_config, user_config, property) {
   var default_config_option = default_config[property];

   var environment_variable = default_config_option.environment_variable;
   if (environment_variable && environment_variable in process.env) {
      return process.env[environment_variable];
   }

   if (property in user_config) {
      return user_config[property];
   }

   return default_config_option.default_value;
}

function resolve_values(default_config, user_config) {
   var values = {};
   for (var property in default_config) {
      values[property] = resolve_value(default_config, user_config, property);
   }
   return values;
}
