'use strict';

var path = require('path');

module.exports = function(controller_directory, models) {
   var controller_import = function() {
      var relative_controller_directory = path.relative(
            __dirname, controller_directory);
      return function(controller) {
         var relative_controller_file = path.join(
               relative_controller_directory, controller);
         return require('./' + relative_controller_file);
      };
   }();

   var Ability = controller_import('ability')(models.Ability);
   // var Alignment = controller_import('alignment')(models.Alignment);
   // var Character = controller_import('character')(
   //       models.Character,
   //       models.CharacterAbility,
   //       models.CharacterLevel,
   //       models.CharacterSkill,
   //       models.Ability,
   //       models.Alignment,
   //       models.Class,
   //       models.Condition,
   //       models.Race,
   //       models.Skill);
   // var Class = controller_import('class')(models.Class);
   // var Condition = controller_import('condition')(models.Condition);
   // var Race = controller_import('race')(models.Race);
   // var Skill = controller_import('skill')(models.Skill, models.Ability, Ability);

   var controllers = [
      Ability,
      // Alignment,
      // Character,
      // Class,
      // Condition,
      // Race,
      // Skill,
   ];

   return new function() {
      this.Ability = Ability;
      // this.Alignment = Alignment;
      // this.Character = Character;
      // this.Class = Class;
      // this.Condition = Condition;
      // this.Race = Race;
      // this.Skill = Skill;

      this.add_routes = function(router) {
         controllers.forEach(function(controller) {
            for (var i in controller.routes) {
               var route = controller.routes[i];
               router[route.method](route.path, route.responder);
            }
         });

         router.get('/seed', function(request, response) {
            for (var i in models) {
               var model = models[i];
               console.log(model, model.associations);
               if (model.__seed) {
                  model.__seed();
               }
            }
            return response.status(201).send('');
         });
      };
   }();
}
